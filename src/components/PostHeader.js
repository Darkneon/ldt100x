import React from 'react'
import Link from "gatsby-link";

class PostHeader extends React.Component {
    render() {
      const title = this.props.title

        return (
            <section id="header">
                <div className="inner">
                    <span className="icon major fa-cloud"></span>
                    <h1>
                      {title}
                    </h1>
                </div>
                <Link to="/">Go back home</Link>
            </section>

        )
    }
}

export default PostHeader
