import React from 'react'

class Header extends React.Component {
    render() {
        return (
            <section id="header">
                <div className="inner">
                    <span className="icon major fa-cloud"></span>
                    <h1>
                        Hi, I'm <strong>Robert</strong>, and welcome to my learning portfolio.
                    </h1>
                    <p>
                      This is my learning portfolio for the course
                      Instructional Design and Technology: Learning Theories.
                    </p>
                    <ul className="actions">
                        <li><a href="#one" className="button scrolly">Discover and Learn</a></li>
                    </ul>
                </div>
            </section>
        )
    }
}

export default Header
