import React from "react";
import Link from "gatsby-link";
import Helmet from "react-helmet";

import ImageDog from '../assets/images/dog.jpg'
import ImageConstructivism from '../assets/images/building.jpg'
import ImageLab from '../assets/images/lab.jpg'
import ImageMemory from '../assets/images/memory.jpg'
import ImagePencils from '../assets/images/pencils.jpg'
import ImageAdult from '../assets/images/adult.jpg'
import ImageLights from '../assets/images/lights.jpg'
import ImageMicroscope from '../assets/images/microscope.jpg'

import Header from "../components/Header";

class Homepage extends React.Component {
    render() {
        const siteTitle = this.props.data.site.siteMetadata.title;

        return (
            <div>
                <Header />
                <Helmet title={siteTitle} />

                <section id="one" className="main style1">
                    <div className="grid-wrapper">
                          <div className="col-12">
                            <header className="major align-center">
                              <h2>About</h2>
                            </header>
                            <p>
                                I am a software developer with over 10 years of experience in the field.
                                I have a keen interest in education and technology and how we can use software to make learning more fun and engaging.
                            </p>
                        </div>
                    </div>
                </section>

                <section id="three" className="main style1 special">
                    <div className="grid-wrapper">
                        <div className="col-12">
                            <header className="major">
                                <h2>Portfolio</h2>
                            </header>
                            <p>
                              A demonstration of my abilities to apply​ learning theories to real-world instructional design scenarios.
                              Whenever possible I will try to relate my learning experiences and scenarios to software development.</p>
                        </div>

                      {/* Personal Learning Experiences */}
                        <div className="col-4">
                            <span className="image fit"><img src={ImageLab} alt="" /></span>
                            <h3>Personal Learning Experiences</h3>
                            <ul className="actions">
                                <li>
                                  <Link to="/personal-learning-experiences" className="button">More</Link>
                                </li>
                            </ul>
                        </div>

                      {/* Behaviorism */}
                        <div className="col-4">
                            <span className="image fit"><img src={ImageDog} alt="" /></span>
                            <h3>Behaviorism</h3>
                            <ul className="actions">
                                <li>
                                  <Link to="/behaviourism" className="button">More</Link>
                                </li>
                            </ul>
                        </div>

                      {/* Constructivism */}
                      <div className="col-4">
                        <span className="image fit"><img src={ImageConstructivism} alt="" /></span>
                        <h3>Constructivism</h3>
                        <ul className="actions">
                          <li>
                            <Link to="/constructivism" className="button">More</Link>
                          </li>
                        </ul>
                      </div>

                      {/* Cognitivism and Connectivism */}
                      <div className="col-4">
                        <span className="image fit"><img src={ImageMemory} alt="" /></span>
                        <h3>Cognitivism and Connectivism </h3>
                        <ul className="actions">
                          <li>
                            <Link to="/cognitivism-and-connectivism" className="button">More</Link>
                          </li>
                        </ul>
                      </div>

                      {/* Andragogy */}
                      <div className="col-4">
                        <span className="image fit"><img src={ImageAdult} alt="" /></span>
                        <h3>Andragogy</h3>
                        <ul className="actions">
                          <li>
                            <Link to="/andragogy" className="button">More</Link>
                          </li>
                        </ul>
                      </div>

                      {/* Complex Models & Authentic Assessment */}
                      <div className="col-4">
                        <span className="image fit"><img src={ImageLights} alt="" /></span>
                        <h3>Complex Models & Authentic Assessment</h3>
                        <ul className="actions">
                          <li>
                            <Link to="/complex-models-authentic-assessment" className="button">More</Link>
                          </li>
                        </ul>
                      </div>


                      {/* Microlearning */}
                      <div className="col-4">
                        <span className="image fit"><img src={ImageMicroscope} alt="" /></span>
                        <h3>Microlearning Project</h3>
                        <ul className="actions">
                          <li>
                            <Link to="/microlearning" className="button">More</Link>
                          </li>
                        </ul>
                      </div>

                      {/* Comparing Learning Theories */}
                      <div className="col-4">
                        <span className="image fit"><img src={ImagePencils} alt="" /></span>
                        <h3>Comparing Learning Theories (Chart)</h3>
                        <ul className="actions">
                          <li>
                            <Link to="/comparing-learning-theories" className="button">More</Link>
                          </li>
                        </ul>
                      </div>
                    </div>
                </section>
            </div>
        );
    }
}

Homepage.propTypes = {
    route: React.PropTypes.object
};

export default Homepage;

export const pageQuery = graphql`
    query IndexQuery {
        site {
            siteMetadata {
                title
            }
        }
    }
`;