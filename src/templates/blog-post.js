import React from "react";
import Helmet from "react-helmet";
import Link from "gatsby-link";
import PostHeader from "../components/PostHeader";

class BlogPostTemplate extends React.Component {
    render() {
        const post = this.props.data.markdownRemark;
        const siteTitle = this.props.data.site.siteMetadata.title;
        const chart = !!post.frontmatter.chart === true ? 'chart' : '';

        return (
            <div>
                <Helmet title={`${post.frontmatter.title} | ${siteTitle}`} />
                <PostHeader title={post.frontmatter.title} />
                <div className={`post ${chart}`} dangerouslySetInnerHTML={{ __html: post.html }} />
            </div>
        );
    }
}

export default BlogPostTemplate;

export const pageQuery = graphql`
    query BlogPostByPath($path: String!) {
        site {
            siteMetadata {
                title
                author
            }
        }
        markdownRemark(frontmatter: { path: { eq: $path } }) {
            id
            html
            frontmatter {
                title    
                chart            
            }
        }
    }
`;
