---
title: Constructivism
path: "/constructivism"
---

![Constructivism Infographic](constructivism.png)

# Learning Scenario

The class "Instructional Design and Technology: Learning Theories" is constructed around many of the concepts of 
Constructivism. 

I find each week's summary very brief and to learn the material deeper I need to do more research on my own. 
As I'm doing my own research, I'm constructing my own knowledge of the theory. Later, when I read other people portfolios
or discussions on the forum that knowledge is re-enforced or challenged. 
Having weekly discussions on the forum makes the learning very social even if it is just reading other people posts. The weekly discussion topics are crafted to be
open-ended and have us reflect on the new knowledge we acquired as well.  
   