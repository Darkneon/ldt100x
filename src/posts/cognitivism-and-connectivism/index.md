---
title: Cognitivism and Connectivism
path: "/cognitivism-and-connectivism"
---

## Cognitivism Overview
Cognitivism is a response to the shortcomings of focusing only on observable behaviors in Behaviorism (Reeve, 2012).
It focuses instead on the changes in mental activities and gives great importance to the role of memory.
It is the change in knowledge and understanding that demonstrates that learning has occurred which might or not be
accompanied with a change in behaviour (Reeve, 2012).

Information processing is an important part of cognitivism as it explains the relationships between
environmental events, encoding of information, memory and new knowledge and how they relate to learning (Schunk, 2016, p. 164).

Information is processed in stages. It starts with sensory information (e.g., visual, auditory) from the environment
and is held temporary in sensory form (e.g., hearing, sight, touch) (Schunk, 2016, p. 165). Then, sensory memory transfers
the information to our short-term memory, which is our in the moment memory with a very small capacity (Schunk, 2016, p. 166). Then,
we relate that knowledge with information that is in out long-term memory which we fetch and place in our short-term memory
(Schunk, 2016, p. 166). Finally, the new and old information are processed and new knowledge is either discarded or transfered back
to long-term memory (Schunk, 2016, p. 166). 

![Information Processing Model](detailed-information-processing-model.jpg)
*Figure 1. (Detailed information Processing Model, n.d)*

Cognitivism deals with Cognitive Load which is how much our information processing system can handle (Schunk, 2016, p. 223). Since our short-term 
memory's capacity is very small we can only handle so many simultaneous stimuli (Schunk, 2016, p. 223).  

There are two types of cognitive load: intrinsic and extrinsic. Intrinsic refers to the many unrelated information
that is required to understand a given material while extrinsic refers to the manner how the material is presented (Schunk, 2016, p. 223-224). 

From an instructional designer perspective, the goal is to always reduce the cognitive load. Richard E. Mayer 
explains 12 principles to reduce cognitive load when designing multimedia instructions in "Research-Based Principles for Designing Multimedia Instruction".  

|Principle|Description|
|---------|-----------|
|Coherence|Delete extraneous material|
|Signaling|Highligh essential material|
|Redundancy|Don’t add onscreen captions to narrated graphics|
|Spatial contiguity|Present spoken words at same time as corresponding graphic|
|Segmenting|Break lesson into learner-paced parts|
|Pre-training|Present characteristics of key concepts before lesson|
|Modality|Use spoken words rather than printed words|
|Personalization|Put words in conversational style rather than formal style|
|Voice|Put words in human	voice rather than machine voice|
|Embodiment|Have onscreen agent use human-like gestures	and	movements|
|Image|Do not necessarily put static image agent on the screens|
*Table 1: 12 Principles for how to design multimedia (Mayer, 2014)*

 
Scaffolding, that we have seen under Constructivism, can help to reduce cognitive load (Schunk, 2016, p. 224).

## Connectivism Overview 
Connectivism states that knowledge is a network of nodes where each node represents a community and where
a community is information shared by people as well as their engagement around a similar topic (Tchoshanov, 2013, p48-49).

Learning happens as the learner actively searches, discovers and adds more nodes to the network (Tchoshanov, 2013, p48-49).
In a way, connectivism is about people discovering and knowing where distributed knowledge is and how to access it.

## Infographic

![Cognitivism and Connectivism](cognitivism-vs-connectivism.png)

## Learning Scenario

I was thinking how learing about the graph data structure in programming would apply to cognitivism 
and connectivism.

From a cognitivism perspective, a teacher first needs to talk about linked list to build a mental
model how to interconnect address locations in computer memory. After, the teacher needs to talk about trees, 
and make the connection to link lists and how a tree is just a collection of linked list. Finally, the teacher can
talk about graphs and make the connection that a tree is just a graph with no loops. 
  
From a connectivism perspective, the learner would need to do his own research about graph and 
discover how graphs are used in video games or databases for example. Then the learner would search for tutorials and
videos about graphs on the Internet. He can also search for existing implementations of graph on Github.com and see
how other people programmed them. 


References:

Mayer, R. (2014). Research-based principles for designing multimedia instruction. Retrieved from: https://hilt.harvard.edu/files/hilt/files/background_reading.pdf

Reeve, C. (2012, January 12). Cognitivism and games. Retrieved July 4, 2018, from http://playwithlearning.com/2012/01/11/cognitivism-and-games/

Schunk, D. H. (2016). Learning theories: An educational perspective. Boston: Pearson.
 
Tchoshanov, M. (2013). Engineering of learning: Conceptualizing e-didactics. Moscow, Russian Federation: UNESCO Institute for Information Technologies in Education.

Detailed information Processing Model [Digital image]. (n.d.). Retrieved from https://www.buzzle.com/images/diagrams/detailed-information-processing-model.jpg