---
title: Andragogy
path: "/andragogy"
---

# Infographic

![Andragogy Infographic](./andragogy.png)

# Learning Experience


This is a learning experience that happened not a long time ago. 
I decided to learn [Pico 8](https://www.lexaloffle.com/pico-8.php) which is a small fantasy console used to program games or graphics 
such as [this one](https://www.youtube.com/watch?v=qMnT-DwQkok).

My learning experience incorporated the principles of andragogy in the following manners.

#### 1. Involved adult learners

I was fully involved in the whole experience. I knew which concepts I needed to learn and in which
order so I made a plan for myself to follow. The plan consisted of tutorials to read after I 
did extensive research using Google. 

#### 2. Adult learners' experience 

Pico 8 uses a programming language called Lua. Even though I have never programmed in this language before, I was able to
use my experience from different programming languages to pick up Lua very quickly.  

#### 3. Problem centered

The learning experience was problem centered because I knew what I wanted to accomplish. 
Even though Pico 8 comes with many features, I ignored all the ones that did not help me to 
solve my problem. 

This was my result at the end.

![Hello](./hello.gif)

