---
title: Comples Models and Authentic Assessment
path: "/complex-models-authentic-assessment"
---

# Learning Scenario

I was think of a learning scenario to initiate kids to programming. It uses Project Based 
Learning (PBL) that put kids in the role of graphics programmers and by the end
have them complete an unfinished computer picture using programming.  
 
# Project Based Learning 

Project Based Learning works by having students work on a project for an extended period of time that 
focuses on solving a real world problem ("What is PBL", n.d). The project must be complex to spur
the students use of their critical thinking, creating and communication skills ("What is PBL". n.d).

The PBL consists of the follow steps. At first, the students are presented with 
an incomplete program that renders different shapes on the computer screen: lines, squares, triangles and circles.
They tinker with the program and each time they make a small modification they recompile the program to observe the result. 
Over time, they learn which part affects what by observing the differences on the computer screen. This introduces 
them to different programming concepts without introducing any formal definition. 

At this point we can introduce the following programming constructs: variables, conditions, loop and functions. The students
are not given the definitions but only what concepts they should search for. As they google for them, 
they will be able to make the connections with the program they tinkered with. 

Here it would be a good idea to give a quiz where students are given a programming construct and they
need to construct a response of how that construct can be used.
 
After, they are put into groups and are tasked with completing the painting by programing more complex objects. They are
not given a specific object to draw instead they are free to pick any object they want.
     
# Authentic Assessment

There are many opportunities for authentic assessment during the project. The main one of course is finishing the project
and explaining how the rendering to the screen is done. When they tinker with the program at the beginning, they get
direct evidence what the program does and if they find similar code elsewhere in the program they can assess if the 
code is doing the same thing or only looks similar. And the quiz as well is an authentic assessment since 
the students need to describe examples with the knowledge they gained.    
 
# Connections to Learning Theories

The part where student tinker with the program is part of the constructivism learning theory. As they modify the 
program, they can compare the new result on the computer screen with the old one and build their own understanding.
They can also discuss and their share their findings with other students. Later, when the concepts are formally
introduced, the students need to process the information. They start to understand the small building blocks of programming and see how they can be used together. Finally,
they use the combination all those different concepts to complete the project. 

References:

What is PBL | Project Based Learning | BIE. (n.d.). Retrieved July 16, 2018, from http://www.bie.org/about/what_pbl

