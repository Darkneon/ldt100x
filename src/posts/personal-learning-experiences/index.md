---
title: Personal Learning Experiences
path: "/personal-learning-experiences"
---

# Middle school
I remember not being very interested in learning the materials we were taught in middle school but I would read a lot of technical books back at home.
I remember when I picked up an old programming book about BASIC from the library one day.
I loved the book for being very easy to read as well as its use of drawings for explanations.
As I was reading the book, I was typing the commands from the book into my computer.
Slowly, I learned how to make a computer do things I wanted it to do and later I started to create my own programs.

# College
College was different than middle and high school. I got accepted into a Computer Science program and I enjoyed going to every single class it offered
because I felt it would help me with my career.
For the first time, taking notes in class was not me going through the motions but I would actually review them after class. I remember it was a shock when I
discovered there are so many different topics in computer science and at the time I just wanted to learn as much as possible about every single one of them.

# Professional
I remember my first job after graduating was very overwhelming at first. The reason was that I had to learn about how the departments
in my company were operating and that was not something we were taught in school. There was also a lot of pressure to learn it as fast as possible too.
I remember having to talk to many different people and I also learned to always clarify assumptions.