---
title: Behaviourism
path: "/behaviourism"
---

## Behaviourism

Behaviourism is a learning theory that focuses on observable and measurable 
changes in behaviour while ignoring any mental thoughts because those are considered unreliable (Reeve, 2012; Schunk, 2016, p. 72). 
It is the change in the behaviour that indicates that some learning has occurred (Reeve, 2012).

John B. Watson is considered the father of Behaviourism because he was the first to argue that
psychology, in order to be considered a true science, needs to follow the foot steps of physical sciences 
and hence why the focus on observable and measurable behaviours only (Schunk, 2016, p. 72).

An important experiment done by Ivan Pavlov, which was later used by Watson to fortify his theory of Behaviorism (Schunk, 2016, p. 72), was 
exposing dogs to an external stimulus when giving them food. Pavlov noticed that when presented with food, the dogs 
would start to salivate. In his experiment, he exposed his dogs to a ticking metronome 
before presenting them the food and over time he observed that the dogs would start salivating to the 
sound of the ticking metronome alone (Schunk, 2016, p. 79). He concluded that this response to the stimulus was not automatic 
but has been learned gradually (Schunk, 2016, p. 78). He called it classical conditioning.
 
Influenced by the work of Watson and Pavlov, Burrhus Frederic Skinner formulated a behavioral theory 
called operant conditioning (Schunk, 2016, p. 88). This theory is based on rewarding or punishing a new behaviour where 
rewarding good behaviour strengths or reinforces the behaviour while punishing bad behaviour reduces its repetition (Reeve, 2012).   
  
## Learning Experience

It turns out none of my personal learning experiences used behaviourism. In my experiences, I did not need any rewards to
make me learn new things rather the drive to learn was mostly intrinsic as I was very self-motivated. For example, 
in middle school I would go to the library because I wanted to learn something that would be useful for me. 

To be grounded in behaviourism, my learning experiences would require a teacher or tutor to show me instructions that
I could follow as a practise. If I'm doing the instructions correctly, the reward could be in the form of feedback
to reinforce that what I am doing is correct. This approach follows a tell-show-practise-reinforce sequence (Reeve, 2012).    

References:

Reeve, C. (2012, January 6). Behaviourism and games. Retrieved July 2, 2018, from http://playwithlearning.com/2012/01/06/behaviourism-and-games/

Schunk, D. H. (2016). Learning theories: An educational perspective. Boston: Pearson.