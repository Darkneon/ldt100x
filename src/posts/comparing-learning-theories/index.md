---
title: Comparing Learning Theories
chart: true
path: "/comparing-learning-theories"
---

<div class="table-wrapper">
    <table style="width:100%">
     <colgroup>
       <col span="1" style="width: 10%;">
       <col span="1" style="width: 15%;">
       <col span="1" style="width: 15%;">
       <col span="1" style="width: 15%;">
       <col span="1" style="width: 15%;">
       <col span="1" style="width: 15%;">
     </colgroup>           
      <tr>
        <th></th>
        <th><span class="icon icon__behaviorism fa-book"></span> Behaviorism</th>
        <th><span class="icon icon__constructivism fa-book"></span> Constructivism</th> 
        <th><span class="icon icon__cognitivism fa-book"></span> Cognitivism</th>
        <th><span class="icon icon__connectivism fa-book"></span> Connectivism</th>
        <th><span class="icon icon__andragogy fa-book"></span> Andragogy</th>
      </tr>           
      <tr>
        <td>
              Summary
        </td>
        <td>
            <ul>
                <li>Focuses on observable behaviours</li>
                <li>Mental thoughts are considered unreliable</li>
                <li>Response due to a stimuli</li>
                <li>Response due to a reward or punishemnt</li>
            </ul>                 
        </td>
        <td>
            <ul>
                <li>New knowledge is create from old knowledge</li>
                <li>Process is more important than the result</li>
                <li>Knowledge is constructed through social interactions and collaboration (Social Constructivism)</li>
            </ul>                 
        </td>
        <td>
            <ul>
                <li>Focuses on mental activities such as memory, thinking and reflection</li>
                <li>The learner connects new knowledge into something meaningful </li>
                <li>The process is more important than the final result</li>                
                <li>Learning is intrinsic</li>                
            </ul>                 
        </td>   
        <td>
            <ul>
                <li>Focuses on social learning from communities</li>
                <li>Knowledge is a network where each node is a community</li>
                <li>Create connections between nodes to create larger networks of knowledge</li>                                 
            </ul>                 
        </td>    
        <td>
            <ul>
                <li>Learning theory of how adults learn</li>
                <li>Problem-centered instead of broad subject</li>
                <li>Adults need less direction from a teacher than children</li>
                <li>Adults rely on their previous experience as a foundation for learning</li>
                <li>Learning is exercised by internal motivations rather than external</li>
                <li>Learning needs to focus on improving something in their lives</li>
            </ul>                 
        </td>    
      </tr> 
      <tr>
          <td>
               Concepts
          </td>
          <td>
              <ul>
                  <li>Classical conditioning</li>
                  <li>Operant consitioning</li>
              </ul>                 
          </td>  
           <td>
              <ul>
                  <li>Scaffolding</li>
                  <li>Zone of Proximal Development</li>
              </ul>                 
          </td>    
           <td>
              <ul>
                  <li>Information processing</li>                  
                  <li>Cognitive load</li>                  
              </ul>                 
          </td>   
          <td>
              <ul>
                 <li>Networks</li>
                 <li>Nodes / Communities</li>
              </ul>                 
          </td>  
          <td>
              <ul>
                  <li>Involved adult learners</li>                  
                  <li>Adult learners' experience</li>                                                      
                  <li>Problem-centered</li>                  
                  <li>Relevance and impact to learners lives</li>                  
              </ul>                 
          </td>    
        </tr> 
        <tr>
            <td>Teacher or Learner Centered</td>
            <td>Teacher</td>
            <td>Learner</td>
            <td>Teacher</td>
            <td>Learner</td>
            <td>Learner</td>
        </tr>
        <tr>
            <td>Criticism</td>
            <td>
                <ul>
                    <li>Ignores any emotional state and self-motivation</li>
                    <li>Cannot explain learning that occurs in the absence of reinforcement (ex: initial language learning)
                </ul>
            </td>
            <td>Learning is not structured which some learners might dislike</td>
            <td>Cognitive load differs for each learner</td>
            <td>Not a theory because does not explain how learning occurs</td>
            <td>Might not be exlusive to adults but could also be applied to children</td>
        </tr>        
    </table>
</div>


Sources: 

Bell, F. (2011, March). Connectivism: Its place in theory-informed research and innovation in technology-enabled learning. Retrieved July 19, 2018, from http://www.irrodl.org/index.php/irrodl/article/view/902/1664

Flores, Reynaldo. (2013, June 23). Behaviorism: Its Strengths and Weaknesses. Retrieved June 13, 2018, from https://reynaldojrflores.wordpress.com/2013/06/23/224/

Pappas, C. (2017, December 21). The Adult Learning Theory - Andragogy - of Malcolm Knowles. Retrieved July 4, 2018, from https://elearningindustry.com/the-adult-learning-theory-andragogy-of-malcolm-knowles

Reeve, C. (2012, January 6). Behaviourism and games. Retrieved July 2, 2018, from http://playwithlearning.com/2012/01/06/behaviourism-and-games/

Reeve, C. (2012, January 12). Cognitivism and games. Retrieved July 4, 2018, from http://playwithlearning.com/2012/01/11/cognitivism-and-games/

Schunk, D. H. (2016). Learning theories: An educational perspective. Boston: Pearson.

Tchoshanov, M. (2013). Engineering of learning: Conceptualizing e-didactics. Moscow, Russian Federation: UNESCO Institute for Information Technologies in Education.
