---
title: Microlearning Project
path: "/microlearning"
---

# Tool


Visme is a web application for creating infographics and it's the tool I used to create all my infographics.
I selected it because it was extremely intuitive after playing with it for the first 15 minutes. Also, it has a vast array of
nice looking templates to chose from and given my limited time I wanted to focus more on the content of the infographics and less on the design. 

Here are the infographics that were created with Visme.
<div class="gallery">
    <span class="thumbnail"> 
        <img src="../constructivism/constructivism.png" />
    </span>        
    <span class="thumbnail"> 
        <img src="../cognitivism-and-connectivism/cognitivism-vs-connectivism.png" />
    </span>    
    <span class="thumbnail"> 
        <img src="../andragogy/andragogy.png" />
    </span>
</div>

<br />

# Connection to Cognitivism

Playing with Visme in the browser would trigger a visual stimulus. From a Cognitivism and an information processing perspective,
that stimulus would register into my short-term memory. As I have experience with Photoshop, a graphics editing tool, I would
fetch from my long-term memory my knowledge of Photoshop. I would then compare my currently acquired knowledge of Visme and compare
it to my knowledge with Photoshop and start making associations between the two. 

# Connection to Andragogy

One of the principle of Andragogy is that learning should be problem centered. Learning Visme was meant to solve my need
to create infographics for the class. Another principle of Andragogy is that learning should have relevance to the learner's life.
Hopefully, one day I will be designing instructions and I will use Visme, or maybe another but similar tool, to create
infographics for content. Finally, and this one crosses over a little with Cognitivism, but I used my previous experience to help
me make associations between Visme and Photoshop.    

# Effectiveness

Learning Visme was straight forward to me. I started to interact with 
the tool and for every action I was pleasantly surprised how it has many of the same concepts as
Photoshop. I was effectively processing the information that was presented to me on the screen, remembering a pattern I 
have seen before and continuing to the next step to repeat the same process. I will conclude that it was very effective
because I was able to create all my infographics without ever reading the documentation or any external resources!
